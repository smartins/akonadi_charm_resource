/*
    Copyright (c) 2011 Sérgio Martins <iamsergio@gmail.com>

    This library is free software; you can redistribute it and/or modify it
    under the terms of the GNU Library General Public License as published by
    the Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This library is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
    License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to the
    Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
    02110-1301, USA.
*/

#include "databasereader.h"
#include "settings.h"

#include <KCalCore/Event>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>

enum Fields {
  FieldId = 0,
  FieldUserId,
  FieldEventId,
  FieldInstallationId,
  FieldReportId,
  FieldTask,
  FieldComment,
  FieldStart,
  FieldEnd,
  FieldTaskName
};

class DatabaseReader::Private  {
public:
  Private( CharmSettings *settings, DatabaseReader *qq ) : mSettings( settings )
                                                         , q( qq )
  {
    mDatabase = QSqlDatabase::addDatabase( "QSQLITE" );
    mDatabase.setDatabaseName( mSettings->databaseFile() );
  }

  Akonadi::Item sqlToEvent( const QSqlQuery &query )
  {
    //const int id              = query.value(FieldId).toString();
    //const int user_id         = query.value(FieldUserId).toInt();
    const int event_id        = query.value(FieldEventId).toInt();
    //const int installation_id = query.value(FieldInstallationId).toInt();
    //const int report_id       = query.value(FieldReportId).toInt();
    //const int task            = query.value(FieldTask).toInt();
    const QString comment     = query.value(FieldComment).toString();
    const QDateTime start     = query.value(FieldStart).toDateTime();
    const QDateTime end       = query.value(FieldEnd).toDateTime();
    const QString taskName    = query.value(FieldTaskName).toString();
    const int duration = start.secsTo( end );

    Akonadi::Item item;
    if ( start.isValid() &&
         end.isValid() &&
         event_id > 0 &&
         duration > mSettings->minimumDuration() &&
         ( !mSettings->from().isValid() || start > mSettings->from() ) ) {
      KCalCore::Event::Ptr event( new KCalCore::Event() );
      event->setUid( QLatin1String( "CharmResource-event_" ) + QString::number( event_id ) ); // TODO:prepend resource name
      event->setAllDay( false );
      event->setDtStart( KDateTime( start, KDateTime::UTC ) );
      event->setDtEnd( KDateTime( end, KDateTime::UTC ) );
      event->setSummary( taskName ); // TODO: task name
      event->setDescription( comment );
      item.setPayload<KCalCore::Event::Ptr>( event );
      item.setMimeType( event->mimeType() );
      item.setRemoteId( QString::number( event_id ) );
    }
    return item;
  }

  QString mDatabaseFile;
  QSqlDatabase mDatabase;
  CharmSettings *mSettings;
private:
  DatabaseReader *q;
};

DatabaseReader::DatabaseReader( CharmSettings *settings,
                                QObject *parent ) : QObject( parent )
                                                  , d( new Private( settings, this ) )

{

}

DatabaseReader::~DatabaseReader()
{
  delete d;
}

void DatabaseReader::loadEvents()
{

  Akonadi::Item::List itemList;
  Result result = ResultSuccess;
  QString errorMessage;
  if ( d->mDatabase.open() ) {
    QSqlQuery query = QSqlQuery( "SELECT e.id,e.user_id,e.event_id,e.installation_id,e.report_id,e.task,e.comment,e.start,e.end,t.name FROM Events e, Tasks t where t.task_id=e.task;", d->mDatabase );

    while( query.next() ) {
      const Akonadi::Item item = d->sqlToEvent( query );
      if ( item.remoteId() > 0 )
        itemList.append ( item );
    }

    if ( query.lastError().isValid() ) {
      result = ResultQueryErrorDB;
      errorMessage = d->mDatabase.lastError().text();
    }
    d->mDatabase.close();
  } else {
    result = ResultErrorOpeningDB;
    errorMessage = d->mDatabase.lastError().text();
  }

  emit eventsLoaded( itemList, result, errorMessage );
}

void DatabaseReader::loadEvent( const Akonadi::Item &item )
{
  if ( d->mDatabase.open() ) {
    QSqlQuery query = QSqlQuery( "SELECT * FROM Events where event_id = " + item.remoteId(),
                                 d->mDatabase );
    Akonadi::Item::List itemList;
    Akonadi::Item item;
    if ( !query.first() )
      item = d->sqlToEvent( query );

    emit eventLoaded( item, ResultSuccess );
    d->mDatabase.close();
  } else {
    emit eventLoaded( Akonadi::Item(), ResultErrorOpeningDB,
                      d->mDatabase.lastError().text() );
  }
}

void DatabaseReader::onDatabaseChanged()
{
  QSqlDatabase::removeDatabase( "QSQLITE" );
  d->mDatabase = QSqlDatabase::addDatabase( "QSQLITE" );
  d->mDatabase.setDatabaseName( d->mSettings->databaseFile() );
}
