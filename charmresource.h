/*
    Copyright (c) 2012 Sérgio Martins <iamsergio@gmail.com>

    This library is free software; you can redistribute it and/or modify it
    under the terms of the GNU Library General Public License as published by
    the Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This library is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
    License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to the
    Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
    02110-1301, USA.
*/

#ifndef CHARMRESOURCE_H
#define CHARMRESOURCE_H

#include "settings.h"
#include "databasereader.h"

#include <Akonadi/Item>
#include <Akonadi/Collection>
#include <Akonadi/ResourceBase>


class CharmResource : public Akonadi::ResourceBase, public Akonadi::AgentBase::Observer
{
  Q_OBJECT
  public:
    CharmResource( const QString &id );
    ~CharmResource();

  public Q_SLOTS:
    virtual void configure( WId windowId );

  private Q_SLOTS:
    void handleEventsLoaded( const Akonadi::Item::List &items, DatabaseReader::Result result,
                             const QString &errorString = QString() );
    void handleEventLoaded( const Akonadi::Item &item, DatabaseReader::Result result,
                            const QString &errorString = QString() );

  protected:
    void retrieveCollections();
    void retrieveItems( const Akonadi::Collection &collection );
    bool retrieveItem( const Akonadi::Item &item, const QSet<QByteArray> &parts );

  private:
    class Private;
    Private *const d;
};

#endif
