/*
    Copyright (c) 2012 Sérgio Martins <iamsergio@gmail.com>

    This library is free software; you can redistribute it and/or modify it
    under the terms of the GNU Library General Public License as published by
    the Free Software Foundation; either version 2 of the License, or (at your
    option) any later version.

    This library is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library General Public
    License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to the
    Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA
    02110-1301, USA.
*/

#include "charmresource.h"

#include "settings.h"
#include "settingsadaptor.h"
#include "configdialog.h"

#include <akonadi/agentfactory.h>
#include <Akonadi/ItemFetchScope>
#include <Akonadi/ChangeRecorder>
#include <akonadi/dbusconnectionpool.h>
#include <Akonadi/EntityDisplayAttribute>

#include <KCalCore/Event>
#include <KLocale>
#include <KWindowSystem>

using namespace Akonadi;

class CharmResource::Private {
public:
  Private( CharmResource *qq ) : mSettings( new CharmSettings( componentData().config() ) )
                               , q( qq )
  {
    mDBReader = new DatabaseReader( mSettings, q );
    q->connect( mDBReader, SIGNAL(eventsLoaded(Akonadi::Item::List,DatabaseReader::Result,QString)),
                q, SLOT(handleEventsLoaded(Akonadi::Item::List,DatabaseReader::Result,QString)) );
    q->connect( mDBReader, SIGNAL(eventLoaded(Akonadi::Item,DatabaseReader::Result,QString)),
                q, SLOT(handleEventLoaded(Akonadi::Item,DatabaseReader::Result,QString)) );

    mOldDatabaseFile = mSettings->databaseFile();
    if ( mOldDatabaseFile.isEmpty() ) {
      emit q->status( Broken, i18n( "No database file specified" ) );
    }
  }

  CharmSettings *mSettings;
  DatabaseReader *mDBReader;
  QString mOldDatabaseFile;
private:
  CharmResource *q;
};

CharmResource::CharmResource( const QString &id )
  : ResourceBase( id ), d( new Private( this ) )
{
  setName( QLatin1String( "Charm Resource" ) );

  changeRecorder()->itemFetchScope().fetchFullPayload();
  changeRecorder()->fetchCollection( true );

  new SettingsAdaptor( d->mSettings );
  DBusConnectionPool::threadConnection().registerObject( QLatin1String( "/Settings" ),
                                                         d->mSettings,
                                                         QDBusConnection::ExportAdaptors );
  //connect( this, SIGNAL(reloadConfiguration()), SLOT(load()) );
  //load();
}

CharmResource::~CharmResource()
{
  delete d;
}

void CharmResource::configure( WId windowId )
{
  ConfigDialog dlg( d->mSettings );
  if ( windowId )
    KWindowSystem::setMainWindow( &dlg, windowId );

  if ( dlg.exec() ) {
    emit configurationDialogAccepted();
  } else {
    emit configurationDialogRejected();
  }

  if ( d->mSettings->databaseFile().isEmpty() ) {
    emit status( Broken, i18n( "No database file specified" ) );
    d->mOldDatabaseFile = d->mSettings->databaseFile();
  } else {
    emit status( Idle, i18n( "Ready" ) );
    if ( d->mSettings->databaseFile() != d->mOldDatabaseFile ) {
      d->mOldDatabaseFile = d->mSettings->databaseFile();
      d->mDBReader->onDatabaseChanged();
      Collection collection;
      collection.setRemoteId( QLatin1String( "default" ) );
      invalidateCache( collection );
      synchronize(); // TODO: weird, why doesn't the collection become empty in akonadiconsole, if the file is invalid?
    }
  }
}

void CharmResource::retrieveCollections()
{
  Collection events;
  events.setRemoteId( QLatin1String( "default" ) ); // only one collection supported
  events.setName( QLatin1String( "Charm" ) );
  events.setParentCollection( Akonadi::Collection::root() );
  events.setContentMimeTypes( QStringList() << "text/calendar" << KCalCore::Event::eventMimeType() );
  events.setRights( Collection::ReadOnly );
  EntityDisplayAttribute * const evendDisplayAttribute = new EntityDisplayAttribute();
  evendDisplayAttribute->setIconName( "Charm" );
  events.addAttribute( evendDisplayAttribute );

  collectionsRetrieved( Collection::List() << events );
}

void CharmResource::retrieveItems( const Akonadi::Collection &collection )
{
  if ( collection.remoteId() == QLatin1String( "default" ) ) {
    emit status( Running, i18n( "Reading events from database." ) );
    d->mDBReader->loadEvents();
  }
}

bool CharmResource::retrieveItem( const Item &item, const QSet<QByteArray> &parts )
{
  Q_UNUSED( parts );
  emit status( Running, i18n( "Reading event from database." ) );
  d->mDBReader->loadEvent( item );
  return true;
}

void CharmResource::handleEventsLoaded( const Akonadi::Item::List &items,
                                        DatabaseReader::Result result, const QString &errorString )
{
  if ( result == DatabaseReader::ResultSuccess ) {
    emit status( Idle, QLatin1String( "Ready" ) );
    itemsRetrieved( items );
  } else {
    cancelTask( errorString );
    emit status( Broken, errorString ); // TODO: ResourceScheduler::taskDone() is overriding the status with ready
  }
}

void CharmResource::handleEventLoaded( const Akonadi::Item &item, DatabaseReader::Result result,
                                       const QString &errorString )
{
  emit status( Idle, QLatin1String( "Ready" ) );
  if ( result == DatabaseReader::ResultSuccess ) {
    itemRetrieved( item );
  } else {
    cancelTask( errorString );
    emit status( Broken, errorString );
  }
}

AKONADI_AGENT_FACTORY( CharmResource, akonadi_charm_resource )

#include "charmresource.moc"
